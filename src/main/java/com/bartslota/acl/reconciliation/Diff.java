package com.bartslota.acl.reconciliation;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

public class Diff<T> {

    private final Collection<T> presentInOldNotPresentInNew;
    private final Collection<T> presentInNewNotPresentInOld;

    Diff(Collection<T> oldOne, Collection<T> newOne) {
        this.presentInOldNotPresentInNew = CollectionUtils.subtract(oldOne, newOne);
        this.presentInNewNotPresentInOld = CollectionUtils.subtract(newOne, oldOne);
    }

    boolean exists() {
        return !presentInNewNotPresentInOld.isEmpty() || !presentInOldNotPresentInNew.isEmpty();
    }

    Collection<T> getPresentJustInOld() {
        return presentInOldNotPresentInNew;
    }

    Collection<T> getPresentJustInNew() {
        return presentInNewNotPresentInOld;
    }


}
