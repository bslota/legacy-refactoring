package com.bartslota.acl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.bartslota.acl.reconciliation.Reconciliation;
import com.bartslota.acl.reconciliation.Reconciliation.Reaction;
import com.bartslota.newmodel.lending.application.LendingFacade;
import com.bartslota.newmodel.lending.infrastructure.NewModelConfig;

@Configuration
@Import(NewModelConfig.class)
public class AclConfiguration {

    @Bean
    LendingACL lendingACL(LendingFacade lendingFacade) {
        return new LendingACL(new Reconciliation<>(Reaction.justLog()), lendingFacade);
    }

}
