package com.bartslota.acl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.bartslota.acl.reconciliation.Reconciliation;
import com.bartslota.acl.toggles.NewModelToggles;
import com.bartslota.legacyrefactoring.dtos.BookDto;
import com.bartslota.legacyrefactoring.dtos.BookRequest;
import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.application.CollectCommand;
import com.bartslota.newmodel.lending.application.LendingFacade;
import com.bartslota.newmodel.lending.application.PlaceOnHoldCommand;
import com.bartslota.newmodel.lending.domain.patron.CollectDuration;
import com.bartslota.newmodel.lending.domain.patron.HoldDuration;
import com.bartslota.newmodel.lending.domain.patron.PatronId;

public class LendingACL {

    private final Reconciliation<BookDto> reconciliation;
    private final LendingFacade lendingFacade;

    public LendingACL(Reconciliation<BookDto> reconciliation, LendingFacade lendingFacade) {
        this.reconciliation = reconciliation;
        this.lendingFacade = lendingFacade;
    }

    public List<BookDto> booksPlacedOnHoldBy(UUID patronId, List<BookDto> oldModelResult) {
        if (NewModelToggles.RECONCILE_AND_USE_NEW_MODEL.isActive()) {
            List<BookDto> newModelResult = BookDto.translateFrom(lendingFacade.booksPlacedOnHoldBy(new PatronId(patronId)));
            reconciliation.compare(toSet(oldModelResult), toSet(newModelResult));
            return newModelResult;
        }
        if (NewModelToggles.RECONCILE_NEW_MODEL.isActive()) {
            List<BookDto> newModelResult = BookDto.translateFrom(lendingFacade.booksPlacedOnHoldBy(new PatronId(patronId)));
            reconciliation.compare(toSet(oldModelResult), toSet(newModelResult));
            return oldModelResult;
        }
        return oldModelResult;
    }

    /**
     * Task #3:
     * Implement this. Make sure tests in LendingACL pass.
     */
    public List<BookDto> booksCurrentlyCollectedBy(UUID patronId, List<BookDto> oldModelResult) {
        if (NewModelToggles.RECONCILE_AND_USE_NEW_MODEL.isActive()) {
            List<BookDto> newModelResult = BookDto.translateFrom(lendingFacade.booksCollectedBy(new PatronId(patronId)));
            reconciliation.compare(toSet(oldModelResult), toSet(newModelResult));
            return newModelResult;
        }
        if (NewModelToggles.RECONCILE_NEW_MODEL.isActive()) {
            List<BookDto> newModelResult = BookDto.translateFrom(lendingFacade.booksCollectedBy(new PatronId(patronId)));
            reconciliation.compare(toSet(oldModelResult), toSet(newModelResult));
            return oldModelResult;
        }
        return oldModelResult;
    }

    private Set<BookDto> toSet(List<BookDto> books) {
        return new HashSet<>(books);
    }

    public void createHold(Integer days, boolean openEndedHold, UUID holderId, UUID bookId) {
        if(days == null) {
            lendingFacade.execute(new PlaceOnHoldCommand(new BookId(bookId), new PatronId(holderId), HoldDuration.openEnded()));
        } else {
            lendingFacade.execute(new PlaceOnHoldCommand(new BookId(bookId), new PatronId(holderId), HoldDuration.forDays(days)));

        }
    }

    public void collect(BookRequest bookRequest) {
        lendingFacade.execute(new CollectCommand(CollectDuration.days(bookRequest.getDays()), new BookId(bookRequest.getBookId()), new PatronId(bookRequest.getHolderId())));
    }
}
