package com.bartslota.legacyrefactoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegacyRefactoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(LegacyRefactoringApplication.class, args);
    }

}
