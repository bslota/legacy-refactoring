package com.bartslota.legacyrefactoring.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bartslota.legacyrefactoring.entities.BookEntity;

public interface BookRepository extends JpaRepository<BookEntity, UUID> {

}
