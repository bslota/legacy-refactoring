package com.bartslota.legacyrefactoring.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bartslota.legacyrefactoring.entities.BookHolderEntity;
import com.bartslota.legacyrefactoring.entities.HolderRentalFeeEntity;

public interface HolderRentalFeeRepository extends JpaRepository<HolderRentalFeeEntity, UUID> {

    HolderRentalFeeEntity findByBookHolderEntity(BookHolderEntity entity);

}
