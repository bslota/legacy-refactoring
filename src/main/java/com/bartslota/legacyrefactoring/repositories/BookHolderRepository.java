package com.bartslota.legacyrefactoring.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bartslota.legacyrefactoring.entities.BookHolderEntity;

public interface BookHolderRepository extends JpaRepository<BookHolderEntity, UUID> {

}
