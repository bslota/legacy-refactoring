package com.bartslota.legacyrefactoring.rabbitmq;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bartslota.acl.LendingACL;
import com.bartslota.legacyrefactoring.dtos.BookRequest;
import com.bartslota.legacyrefactoring.services.BookHolderService;
import com.bartslota.legacyrefactoring.services.BookService;

@Component
public class QueueListener {

    private final BookService bookService;
    private final BookHolderService bookHolderService;
    private final LendingACL lendingACL;

    public QueueListener(BookService bookService, BookHolderService bookHolderService, LendingACL lendingACL) {
        this.bookService = bookService;
        this.bookHolderService = bookHolderService;
        this.lendingACL = lendingACL;
    }

    @Transactional
    public boolean collect(BookRequest bookRequest) {
        try {
            bookService.removeHold(bookRequest.getHolderId(), bookRequest.getBookId());
            bookHolderService.createCollectedBook(bookRequest.getHolderId(), bookRequest.getBookId(), bookRequest.getDays());
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            lendingACL.collect(bookRequest);
        }

    }

}
