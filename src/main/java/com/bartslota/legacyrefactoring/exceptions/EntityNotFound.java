package com.bartslota.legacyrefactoring.exceptions;

public class EntityNotFound extends RuntimeException {

    public EntityNotFound() {
        super();
    }
}
