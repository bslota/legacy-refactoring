package com.bartslota.legacyrefactoring.exceptions;

public class InvalidBookCollectionStateException extends RuntimeException {

    public InvalidBookCollectionStateException(String msg) {
        super(msg);
    }
}
