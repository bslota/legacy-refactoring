package com.bartslota.legacyrefactoring.exceptions;

public class InvalidBookLendingStateException extends RuntimeException {

    public InvalidBookLendingStateException(String msg) {
        super(msg);
    }
}
