package com.bartslota.legacyrefactoring.controllers;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.bartslota.acl.LendingACL;
import com.bartslota.legacyrefactoring.dtos.BookDto;
import com.bartslota.legacyrefactoring.dtos.BookRequest;
import com.bartslota.legacyrefactoring.entities.BookEntity;
import com.bartslota.legacyrefactoring.services.BookHolderService;
import com.bartslota.legacyrefactoring.services.BookService;

import static java.util.stream.Collectors.toList;

@Controller
public class BookController {

    private final BookService bookService;
    private final BookHolderService bookHolderService;
    private final LendingACL lendingACL;

    public BookController(BookService bookService, BookHolderService bookHolderService, LendingACL lendingACL) {
        this.bookService = bookService;
        this.bookHolderService = bookHolderService;
        this.lendingACL = lendingACL;
    }

    @PostMapping("/books/holds")
    public ResponseEntity<?> addHold(@RequestBody BookRequest bookRequest) {
        bookService.createHold(bookRequest.getDays(), bookRequest.isOpenEndedHold(), bookRequest.getHolderId(), bookRequest.getBookId());
        lendingACL.createHold(bookRequest.getDays(), bookRequest.isOpenEndedHold(), bookRequest.getHolderId(), bookRequest.getBookId());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/books/collections")
    @Transactional
    public ResponseEntity<?> collect(@RequestBody BookRequest bookRequest) {
        bookService.removeHold(bookRequest.getHolderId(), bookRequest.getBookId());
        bookHolderService.createCollectedBook(bookRequest.getHolderId(), bookRequest.getBookId(), bookRequest.getDays());
        lendingACL.collect(bookRequest);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/books")
    @Transactional
    public ResponseEntity<?> changeState(@RequestBody BookDto bookDto) {
        bookService.changeBookState(bookDto.getIsbn(), bookDto.getBookId(), bookDto.getBookState(), bookDto.getPricePerDay(), bookDto.getBookLendingState());
        return ResponseEntity.ok().build();
    }

    @PostMapping("/titles")
    @Transactional
    public ResponseEntity<?> changeDescription(@RequestBody BookDto bookDto) {
        bookService.changeDesc(bookDto.getBookId(), bookDto.getTitle(), bookDto.getAuthor());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/collectBook")
    //return book to a library
    public ResponseEntity<?> removeCollectedBook(@RequestBody BookRequest bookRequest) {
        bookHolderService.removeCollectedBook(bookRequest.getHolderId(), bookRequest.getBookId());
        return ResponseEntity.ok().build();
    }


    @GetMapping("/holds/{holderId}")
    @Transactional
    public ResponseEntity<List<BookDto>> getPlacedOnHoldBooks(@RequestParam UUID holderId) {
        Set<BookEntity> books = bookHolderService.getBooks(holderId);
        List<BookDto> oldModel = books.stream().map(BookDto::from).filter(dto -> dto.getBookLendingState() == BookEntity.BookLendingState.OnHold).collect(toList());
        return ResponseEntity.ok(lendingACL.booksPlacedOnHoldBy(holderId, oldModel));

    }

    @GetMapping("/books/{holderId}")
    @Transactional
    public ResponseEntity<List<BookDto>> getCollectedBooks(@RequestParam UUID holderId) {
        Set<BookEntity> books = bookHolderService.getBooks(holderId);
        List<BookDto> oldModel = books.stream().map(BookDto::from).filter(dto -> dto.getBookLendingState() == BookEntity.BookLendingState.Collected).collect(toList());
        return ResponseEntity.ok(lendingACL.booksCurrentlyCollectedBy(holderId, oldModel));


    }
}


