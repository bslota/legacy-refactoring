package com.bartslota.newmodel.catalogue;

import java.util.UUID;

import lombok.NonNull;

public final record BookId(@NonNull UUID value) {

}
