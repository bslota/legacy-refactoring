package com.bartslota.newmodel;

public interface DomainEvents {

    void publish(DomainEvent domainEvent);
}
