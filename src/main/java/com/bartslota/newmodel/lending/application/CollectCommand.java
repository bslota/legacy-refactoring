package com.bartslota.newmodel.lending.application;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.patron.CollectDuration;
import com.bartslota.newmodel.lending.domain.patron.PatronId;

import lombok.NonNull;
import lombok.Value;

@Value
public class CollectCommand {

    @NonNull
    CollectDuration collectDuration;
    @NonNull
    BookId bookId;
    @NonNull
    PatronId patronId;
}
