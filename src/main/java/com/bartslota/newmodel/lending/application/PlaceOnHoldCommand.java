package com.bartslota.newmodel.lending.application;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.patron.HoldDuration;
import com.bartslota.newmodel.lending.domain.patron.PatronId;

import lombok.NonNull;
import lombok.Value;

@Value
public class PlaceOnHoldCommand {

    @NonNull
    BookId bookId;
    @NonNull
    PatronId patronId;
    @NonNull
    HoldDuration holdDuration;
}
