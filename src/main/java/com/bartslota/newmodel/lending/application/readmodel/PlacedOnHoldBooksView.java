package com.bartslota.newmodel.lending.application.readmodel;

import java.util.ArrayList;
import java.util.List;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.patron.PatronId;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class PlacedOnHoldBooksView {

    PlacedOnHoldBooksView(PatronId patronId) {
        this.patronId = patronId;
    }

    private PatronId patronId;

    @Getter
    private List<BookId> books = new ArrayList<>();

    void addBook(BookId book) {
        books.add(book);
    }

    void removeBook(BookId book) {
        books.remove(book);
    }

}


