package com.bartslota.newmodel.lending.application;

public enum Result {

    Allowance, Rejection
}
