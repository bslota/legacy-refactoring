package com.bartslota.newmodel.lending.application;

import java.util.Optional;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.book.AvailableBook;

public interface FindAvailableBook {

    Optional<AvailableBook> find(BookId bookId);
}
