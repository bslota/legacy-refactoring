package com.bartslota.newmodel.lending.application.readmodel;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.event.EventListener;

import com.bartslota.newmodel.lending.domain.patron.PatronId;
import com.bartslota.newmodel.lending.domain.patron.events.BookCollected;
import com.bartslota.newmodel.lending.domain.patron.events.BookPlacedOnHold;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LendingQueryFacade {

    private final Map<PatronId, PlacedOnHoldBooksView> holds = new HashMap<>();
    private final Map<PatronId, CollectedBooksView> collectedBooks = new HashMap<>();

    @EventListener
    public void placedOnHold(BookPlacedOnHold bookPlacedOnHold) {
        addBookToHoldBooksView(bookPlacedOnHold);
    }

    @EventListener
    public void bookCollected(BookCollected bookCollected) {
        addBookToCollectedBooksView(bookCollected);
        removeBookFromHoldView(bookCollected);
    }

    public PlacedOnHoldBooksView placedOnHoldBy(PatronId patronId) {
        return holds.get(patronId);
    }

    public CollectedBooksView collectedBy(PatronId patronId) {
        return collectedBooks.get(patronId);
    }

    private void addBookToHoldBooksView(BookPlacedOnHold bookPlacedOnHold) {
        PlacedOnHoldBooksView view = holds.getOrDefault(bookPlacedOnHold.patronId(), new PlacedOnHoldBooksView(bookPlacedOnHold.patronId()));
        view.addBook(bookPlacedOnHold.bookId());
        holds.put(bookPlacedOnHold.patronId(), view);
    }

    private void removeBookFromHoldView(BookCollected bookCollected) {
        holds.get(bookCollected.patronId()).removeBook(bookCollected.bookId());
    }

    private void addBookToCollectedBooksView(BookCollected bookCollected) {
        CollectedBooksView view = collectedBooks.getOrDefault(bookCollected.patronId(), new CollectedBooksView(bookCollected.patronId()));
        view.addBook(bookCollected.bookId());
        collectedBooks.put(bookCollected.patronId(), view);
    }

}
