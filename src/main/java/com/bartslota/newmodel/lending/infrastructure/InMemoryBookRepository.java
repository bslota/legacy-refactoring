package com.bartslota.newmodel.lending.infrastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.context.event.EventListener;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.application.FindAvailableBook;
import com.bartslota.newmodel.lending.domain.book.AvailableBook;
import com.bartslota.newmodel.lending.domain.book.Book;
import com.bartslota.newmodel.lending.domain.book.BookRepository;
import com.bartslota.newmodel.lending.domain.patron.events.BookPlacedOnHold;

public class InMemoryBookRepository implements FindAvailableBook, BookRepository {

    private final Map<BookId, Book> database = new HashMap<>();

    @Override
    public Optional<AvailableBook> find(BookId bookId) {
        return Optional.ofNullable(database.get(bookId)).flatMap(Book::toAvailableBook);
    }

    @Override
    public Optional<Book> findById(BookId bookId) {
        return Optional.ofNullable(database.get(bookId));
    }

    @Override
    public void save(Book book) {
        database.put(book.getBookId(), book);
    }

    @EventListener
    public void handle(BookPlacedOnHold bookPlacedOnHold) {
        database.get(bookPlacedOnHold.bookId()).handle(bookPlacedOnHold);
    }
}
