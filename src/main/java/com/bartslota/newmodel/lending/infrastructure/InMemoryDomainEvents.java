package com.bartslota.newmodel.lending.infrastructure;

import org.springframework.context.ApplicationEventPublisher;

import com.bartslota.newmodel.DomainEvent;
import com.bartslota.newmodel.DomainEvents;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class InMemoryDomainEvents implements DomainEvents {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void publish(DomainEvent domainEvent) {
        applicationEventPublisher.publishEvent(domainEvent);
    }
}

