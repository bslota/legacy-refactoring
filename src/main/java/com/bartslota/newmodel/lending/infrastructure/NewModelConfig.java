package com.bartslota.newmodel.lending.infrastructure;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bartslota.newmodel.DomainEvents;
import com.bartslota.newmodel.lending.application.CollectService;
import com.bartslota.newmodel.lending.application.LendingFacade;
import com.bartslota.newmodel.lending.application.PlaceOnHoldService;
import com.bartslota.newmodel.lending.application.readmodel.LendingQueryFacade;
import com.bartslota.newmodel.lending.domain.patron.PatronRepository;

@Configuration
public class NewModelConfig {

    @Bean
    LendingFacade lendingFacade(CollectService collectService, PlaceOnHoldService placeOnHoldService, LendingQueryFacade lendingQueryFacade) {
        return new LendingFacade(collectService, placeOnHoldService, lendingQueryFacade);
    }

    @Bean
    LendingQueryFacade lendingQueryFacade() {
        return new LendingQueryFacade();
    }

    @Bean
    PatronRepository patronRepository() {
        return new InMemoryPatronRepository();
    }

    @Bean
    DomainEvents domainEvents(ApplicationEventPublisher applicationEventPublisher) {
        return new InMemoryDomainEvents(applicationEventPublisher);
    }

    @Bean
    InMemoryBookRepository bookNewModelRepository() {
        return new InMemoryBookRepository();
    }

    @Bean
    PlaceOnHoldService placeOnHoldService(PatronRepository patronRepository, InMemoryBookRepository bookRepository, DomainEvents domainEvents) {
        return new PlaceOnHoldService(patronRepository, bookRepository, domainEvents);
    }

    @Bean
    CollectService collectService(PatronRepository patronRepository, DomainEvents domainEvents) {
        return new CollectService(patronRepository, domainEvents);
    }
}
