package com.bartslota.newmodel.lending.infrastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.bartslota.newmodel.lending.domain.patron.Patron;
import com.bartslota.newmodel.lending.domain.patron.PatronId;
import com.bartslota.newmodel.lending.domain.patron.PatronRepository;

public class InMemoryPatronRepository implements PatronRepository {

    private final Map<PatronId, Patron> database = new HashMap<>();

    @Override
    public Optional<Patron> findById(PatronId patronId) {
        return Optional.ofNullable(database.get(patronId));
    }

    @Override
    public void save(Patron patron) {
        database.put(patron.getPatronId(), patron);
    }
}
