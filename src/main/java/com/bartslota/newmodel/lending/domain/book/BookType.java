package com.bartslota.newmodel.lending.domain.book;

public enum BookType {
    Restricted, Circulating
}
