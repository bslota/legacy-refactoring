package com.bartslota.newmodel.lending.domain.book;

import java.util.Optional;

import com.bartslota.newmodel.catalogue.BookId;

public interface BookRepository {

    Optional<Book> findById(BookId bookId);

    void save(Book book);

}


