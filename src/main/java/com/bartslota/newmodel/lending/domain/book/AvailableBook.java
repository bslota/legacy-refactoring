package com.bartslota.newmodel.lending.domain.book;

import com.bartslota.newmodel.catalogue.BookId;

import lombok.NonNull;

public final record AvailableBook(@NonNull BookId bookId,
                                  @NonNull BookType bookType) {

    public boolean isRestricted() {
        return bookType.equals(BookType.Restricted);
    }
}

