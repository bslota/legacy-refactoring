package com.bartslota.newmodel.lending.domain.patron;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.book.AvailableBook;
import com.bartslota.newmodel.lending.domain.patron.events.BookCollected;
import com.bartslota.newmodel.lending.domain.patron.events.BookPlacedOnHold;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import static com.bartslota.newmodel.lending.domain.patron.PatronType.Regular;
import static com.bartslota.newmodel.lending.domain.patron.PatronType.Researcher;
import static java.util.stream.Collectors.toSet;

/**
 * TODO: to be implemented
 */
@AllArgsConstructor
public class Patron {

    @NonNull
    @Getter
    private final PatronId patronId;

    @NonNull
    private final PatronType patronType;

    @NonNull
    private Holds holds;

    @NonNull
    private OverdueCollectedBooks overdueCollectedBooks;

    private static final PlacingOnHoldPolicy PLACING_ON_HOLD_POLICIES = PlacingOnHoldPolicy.allPolicies();

    public static Patron patron(PatronType patronType, PatronId patronId) {
        return new Patron(patronId, patronType, Holds.empty(), OverdueCollectedBooks.empty());
    }

    public Optional<BookPlacedOnHold> placeOnHold(AvailableBook book, HoldDuration holdDuration) {
        if (PLACING_ON_HOLD_POLICIES.canPlaceOnHold(book, this, holdDuration)) {
            holds = holds.with(book.bookId());
            return Optional.of(new BookPlacedOnHold(book.bookId(), this.patronId, holdDuration.getDays().orElse(null)));
        }
        return Optional.empty();
    }

    public Optional<BookCollected> collect(BookId bookId, CollectDuration collectDuration) {
        if (holds.contains(bookId)) {
            return Optional.of(new BookCollected(bookId, this.patronId, collectDuration.period().getDays()));
        }
        return Optional.empty();
    }

    boolean isRegular() {
        return Regular.equals(patronType);
    }

    int numberOfHolds() {
        return holds.size();
    }

    int numberOfOverdueBooks() {
        return overdueCollectedBooks.count();
    }

    static final class Holds {

        private final Set<BookId> bookIds;

        private Holds(Set<BookId> bookIds) {
            this.bookIds = bookIds;
        }

        static Holds empty() {
            return new Holds(Collections.emptySet());
        }

        Holds with(BookId bookId) {
            Set<BookId> newBookIds = new HashSet<>(bookIds);
            newBookIds.add(bookId);
            return new Holds(newBookIds);
        }

        int size() {
            return bookIds.size();
        }

        boolean contains(BookId bookId) {
            return bookIds.contains(bookId);
        }
    }

    static final class OverdueCollectedBooks {

        private Set<BookId> overdueBooks;

        private OverdueCollectedBooks(Set<BookId> overdueBooks) {
            this.overdueBooks = overdueBooks;
        }

        static OverdueCollectedBooks empty() {
            return new OverdueCollectedBooks(Collections.emptySet());
        }

        static OverdueCollectedBooks from(BookId... bookIds) {
            return new OverdueCollectedBooks(Arrays.stream(bookIds).collect(toSet()));
        }

        int count() {
            return overdueBooks.size();
        }
    }
}
