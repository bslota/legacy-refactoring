package com.bartslota.newmodel.lending.domain.patron.events;

import com.bartslota.newmodel.DomainEvent;
import com.bartslota.newmodel.catalogue.BookId;
import com.bartslota.newmodel.lending.domain.patron.PatronId;

public record BookPlacedOnHold(BookId bookId,
                               PatronId patronId,
                               Integer forDays) implements DomainEvent {

}
