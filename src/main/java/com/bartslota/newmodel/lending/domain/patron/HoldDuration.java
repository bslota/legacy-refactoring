package com.bartslota.newmodel.lending.domain.patron;

import java.time.Period;
import java.util.Optional;

import lombok.NonNull;

public interface HoldDuration {

    static HoldDuration forTenDays() {
        return forDays(10);
    }

    static HoldDuration forDays(int days) {
        return new CloseEndedHoldDuration(Period.ofDays(days));
    }

    static HoldDuration openEnded() {
        return new OpenEndedHoldDuration();
    }

    Optional<Integer> getDays();

    default boolean isCloseEnded() {
        return getDays().isPresent();
    }
}

final record CloseEndedHoldDuration(@NonNull Period period) implements HoldDuration {

    @Override
    public Optional<Integer> getDays() {
        return Optional.of(period.getDays());
    }
}

final class OpenEndedHoldDuration implements HoldDuration {

    @Override
    public Optional<Integer> getDays() {
        return Optional.empty();
    }
}