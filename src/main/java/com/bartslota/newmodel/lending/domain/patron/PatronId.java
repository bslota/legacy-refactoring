package com.bartslota.newmodel.lending.domain.patron;

import java.util.UUID;

import lombok.NonNull;

public final record PatronId(@NonNull UUID value) {

}
