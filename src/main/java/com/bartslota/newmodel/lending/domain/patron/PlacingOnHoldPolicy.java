package com.bartslota.newmodel.lending.domain.patron;

import java.util.Arrays;
import java.util.List;

import com.bartslota.newmodel.lending.domain.book.AvailableBook;

/**
 * @author bslota on 07/12/2021
 */
@FunctionalInterface
interface PlacingOnHoldPolicy {

    boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration);

    static PlacingOnHoldPolicy allPolicies() {
        return new CompositePlaceOnHoldPolicy(new RegularPatronCannotHoldRestrictedBooks(),
                new MaximumHoldsPolicy(),
                new CannotPlaceOnHoldWhenTwoOverdueBooks(),
                new OnlyResearcherPatronCanPlaceOpenEndedHold());
    }
}

class RegularPatronCannotHoldRestrictedBooks implements PlacingOnHoldPolicy {

    @Override
    public boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration) {
        return !patron.isRegular() || !availableBook.isRestricted();
    }
}

class MaximumHoldsPolicy implements PlacingOnHoldPolicy {

    @Override
    public boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration) {
        return patron.numberOfHolds() < 5;
    }
}

class CannotPlaceOnHoldWhenTwoOverdueBooks implements PlacingOnHoldPolicy {

    @Override
    public boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration) {
        return patron.numberOfOverdueBooks() < 2;
    }
}

class OnlyResearcherPatronCanPlaceOpenEndedHold implements PlacingOnHoldPolicy {

    @Override
    public boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration) {
        if (holdDuration.isCloseEnded()) {
            return true;
        } else {
            return !patron.isRegular();
        }
    }
}

class CompositePlaceOnHoldPolicy implements PlacingOnHoldPolicy {

    private final List<PlacingOnHoldPolicy> policies;

    CompositePlaceOnHoldPolicy(PlacingOnHoldPolicy... policies) {
        this.policies = Arrays.asList(policies);
    }

    @Override
    public boolean canPlaceOnHold(AvailableBook availableBook, Patron patron, HoldDuration holdDuration) {
        return policies.stream().allMatch(policy -> policy.canPlaceOnHold(availableBook, patron, holdDuration));
    }
}