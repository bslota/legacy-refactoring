package com.bartslota.newmodel.lending.domain.patron;

import java.time.Period;

import lombok.NonNull;

public final record CollectDuration(@NonNull Period period) {

    public static CollectDuration forOneMonth() {
        return new CollectDuration(Period.ofMonths(1));
    }

    public CollectDuration {
        if (period.getDays() > 60) {
            throw new IllegalArgumentException("Can checkout max for 60 days");
        }
    }

    public static CollectDuration days(Integer days) {
        return new CollectDuration(Period.ofDays(days));
    }

}
