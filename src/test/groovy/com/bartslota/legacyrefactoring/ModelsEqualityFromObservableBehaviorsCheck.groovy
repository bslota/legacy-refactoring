package com.bartslota.legacyrefactoring

import com.bartslota.acl.AclConfiguration
import com.bartslota.acl.toggles.NewModelToggles
import com.bartslota.legacyrefactoring.controllers.BookController
import com.bartslota.legacyrefactoring.dtos.BookDto
import com.bartslota.legacyrefactoring.dtos.BookRequest
import com.bartslota.legacyrefactoring.entities.BookEntity
import com.bartslota.legacyrefactoring.entities.BookHolderEntity
import com.bartslota.legacyrefactoring.exceptions.InvalidBookCollectionStateException
import com.bartslota.legacyrefactoring.exceptions.InvalidBookLendingStateException
import com.bartslota.legacyrefactoring.rabbitmq.QueueListener
import com.bartslota.legacyrefactoring.repositories.BookRepository
import com.bartslota.legacyrefactoring.services.HolderRentalFeeService
import org.junit.Rule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.togglz.junit.TogglzRule
import spock.lang.Specification

import java.time.Duration
import java.time.Instant
import java.util.stream.Collectors

import static org.assertj.core.api.Assertions.assertThat

@SpringBootTest(classes = [AclConfiguration, LegacyRefactoringApplication])
/**
 * TODO: Implement check between models
 *  a) call observable behavior and use old model
 *  b) call observable behavior and use new model
 *  c) compare results -> test if there are equal
 */
class ModelsEqualityFromObservableBehaviorsCheck extends Specification {

    @Autowired
    private BookController bookController

    @Autowired
    private Fixtures fixtures

    @Autowired
    private HolderRentalFeeService holderRentalFeeService

    @Autowired
    private BookRepository bookRepository

    @Autowired
    private QueueListener queueListener

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(NewModelToggles.class)

    def "regular patron cannot hold restricted books"() {
        given:
            BookEntity restrictedBook = fixtures.aRestrictedBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToHoldBook(aRegularPatron, restrictedBook)

        then:
            thrown InvalidBookLendingStateException
    }


    def "researcher patron can hold restricted books"() {
        given:
            BookEntity restrictedBook = fixtures.aRestrictedBookAvailableForLending()

        and:
            BookHolderEntity aResearcherPatron = fixtures.aResearcherPatron()

        when:
            patronWantsToHoldBook(aResearcherPatron, restrictedBook)

        then:
            oldModelPlacedOnHoldsBooksBy(aResearcherPatron).containsAll(newModelPlacedOnHoldsBooksBy(aResearcherPatron))
    }


    def "patron can hold a book"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        then:
            oldModelPlacedOnHoldsBooksBy(aRegularPatron).containsAll(newModelPlacedOnHoldsBooksBy(aRegularPatron))
    }


    def "patron can hold up to 5 books"() {
        given:
            BookEntity circulatedBook1 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatedBook2 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatedBook3 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatedBook4 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatedBook5 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatedBook6 = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook1, circulatedBook2, circulatedBook3, circulatedBook4, circulatedBook5)

        when:
            patronWantsToHoldBook(aRegularPatron, circulatedBook6)

        then:
            thrown InvalidBookLendingStateException
    }


    def "researcher can place open ended holds"() {
        given:
            BookEntity aCirculatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aResearcherPatron = fixtures.aResearcherPatron()

        when:
            patronWantsToHoldBookForOpenEndedHold(aResearcherPatron, aCirculatingBook)

        then:
            oldModelPlacedOnHoldsBooksBy(aResearcherPatron).containsAll(newModelPlacedOnHoldsBooksBy(aResearcherPatron))
    }


    def "regular patron cannot place open ended holds"() {
        given:
            BookEntity aCirculatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToHoldBookForOpenEndedHold(aRegularPatron, aCirculatingBook)

        then:
            thrown InvalidBookLendingStateException
    }


    def "patron cannot hold books when there are two overdue collections"() {
        given:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronHasTwoCollectedBooksTillYesterday(aRegularPatron)

        and:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            patronWantsToHoldBook(aRegularPatron, book)

        then:
            thrown InvalidBookLendingStateException

    }


    def "patron can hold books when there is one overdue collection"() {
        given:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            thereIsABookCollectedTillYesterdayBy(aRegularPatron)

        and:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            patronWantsToHoldBook(aRegularPatron, book)

        then:
            oldModelPlacedOnHoldsBooksBy(aRegularPatron).containsAll(newModelPlacedOnHoldsBooksBy(aRegularPatron))

    }


    def "cannot place on hold book which is held by someone else"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            BookHolderEntity anotherRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        when:
            patronWantsToHoldBook(anotherRegularPatron, circulatedBook)

        then:
            thrown InvalidBookLendingStateException
    }


    def "can collect a book"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        when:
            patronWantsToCollectTheBook(aRegularPatron, circulatedBook, 5)

        then:
            oldModelCollectedBooksBy(aRegularPatron).containsAll(newModelCollectedBooksBy(aRegularPatron))
            oldModelPlacedOnHoldsBooksBy(aRegularPatron).containsAll(oldModelPlacedOnHoldsBooksBy(aRegularPatron))
    }


    def "cannot collect book not placed on hold"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToCollectTheBook(aRegularPatron, circulatedBook, 5)

        then:
            thrown InvalidBookCollectionStateException

    }


    def "book can be collected for up to 60 days"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        when:
            patronWantsToCollectTheBook(aRegularPatron, circulatedBook, 61)

        then:
            thrown InvalidBookCollectionStateException
    }

    /**
     * This is not implemented - we did not touch "Return" command.
     */

    def "can return a book"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        and:
            patronWantsToCollectTheBook(aRegularPatron, circulatedBook, 5)

        when:
            patronWantsToReturnTheBook(aRegularPatron, circulatedBook)

        then:
            oldModelCollectedBooksBy(aRegularPatron).isEmpty()
            oldModelCollectedBooksBy(aRegularPatron).isEmpty()
    }


    def "can collect a book listening to RabbitMq"() {
        given:
            BookEntity circulatedBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatedBook)

        when:
            queueListener.collect(new BookRequest(circulatedBook.getId(), aRegularPatron.getId(), 10, false))

        then:
            oldModelCollectedBooksBy(aRegularPatron).containsAll(newModelCollectedBooksBy(aRegularPatron))
            oldModelCollectedBooksBy(aRegularPatron).containsAll(newModelPlacedOnHoldsBooksBy(aRegularPatron))
    }

    void patronWantsToHoldBook(BookHolderEntity patron, BookEntity... books) {
        Arrays.asList(books).forEach(book -> bookController.addHold(new BookRequest(book.getId(), patron.getId(), 10, false)))
    }

    void patronWantsToHoldBookForOpenEndedHold(BookHolderEntity patron, BookEntity... books) {
        Arrays.asList(books).forEach(book -> bookController.addHold(new BookRequest(book.getId(), patron.getId(), null, true)))
    }

    void patronWantsToCollectTheBook(BookHolderEntity patron, BookEntity book, int days) {
        bookController.collect(new BookRequest(book.getId(), patron.getId(), days, true))
    }

    void patronWantsToReturnTheBook(BookHolderEntity patron, BookEntity book) {
        bookController.removeCollectedBook(new BookRequest(book.getId(), patron.getId(), null, null))
    }

    List<UUID> oldModelPlacedOnHoldsBooksBy(BookHolderEntity aRegularPatron) {
        return bookController.getPlacedOnHoldBooks(aRegularPatron.getId()).getBody().stream().map(BookDto::getBookId).collect(Collectors.toList())
    }

    List<UUID> newModelPlacedOnHoldsBooksBy(BookHolderEntity aRegularPatron) {
        togglzRule.enable(NewModelToggles.RECONCILE_AND_USE_NEW_MODEL)
        List<UUID> collect = oldModelPlacedOnHoldsBooksBy(aRegularPatron)
        togglzRule.disable(NewModelToggles.RECONCILE_AND_USE_NEW_MODEL)
        return collect
    }

    List<UUID> oldModelCollectedBooksBy(BookHolderEntity aRegularPatron) {
        return bookController.getCollectedBooks(aRegularPatron.getId()).getBody().stream().map(BookDto::getBookId).collect(Collectors.toList())
    }

    List<UUID> newModelCollectedBooksBy(BookHolderEntity aRegularPatron) {
        togglzRule.enable(NewModelToggles.RECONCILE_AND_USE_NEW_MODEL)
        List<UUID> collect = oldModelCollectedBooksBy(aRegularPatron)
        togglzRule.disable(NewModelToggles.RECONCILE_AND_USE_NEW_MODEL)
        return collect
    }


    void patronHasTwoCollectedBooksTillYesterday(BookHolderEntity aRegularPatron) {
        thereIsABookCollectedTillYesterdayBy(aRegularPatron)
        thereIsABookCollectedTillYesterdayBy(aRegularPatron)
    }

    void thereIsABookCollectedTillYesterdayBy(BookHolderEntity aRegularPatron) {
        BookEntity bookEntity = fixtures.aCirculatingBookAvailableForLending()
        patronWantsToHoldBook(aRegularPatron, bookEntity)
        patronWantsToCollectTheBook(aRegularPatron, bookEntity, 1)
        bookEntity.setCollectedFrom(Instant.now().minus(Duration.ofDays(2)))
        bookEntity.setCollectedTill(Instant.now().minus(Duration.ofDays(1)))
        bookEntity.setLendingState(BookEntity.BookLendingState.Collected)
        bookRepository.save(bookEntity)
    }

}


