package com.bartslota.legacyrefactoring

import com.bartslota.legacyrefactoring.entities.BookEntity
import com.bartslota.legacyrefactoring.entities.BookHolderEntity
import com.bartslota.legacyrefactoring.repositories.BookHolderRepository
import com.bartslota.legacyrefactoring.repositories.BookRepository
import com.bartslota.newmodel.catalogue.BookId
import com.bartslota.newmodel.lending.domain.book.Book
import com.bartslota.newmodel.lending.domain.book.BookType
import com.bartslota.newmodel.lending.domain.patron.PatronId
import com.bartslota.newmodel.lending.domain.patron.PatronRepository
import com.bartslota.newmodel.lending.domain.patron.PatronType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import static com.bartslota.legacyrefactoring.entities.BookEntity.BookLendingState.Available
import static com.bartslota.legacyrefactoring.entities.BookEntity.BookState.InLending
import static com.bartslota.legacyrefactoring.entities.BookEntity.BookState.JustInCatalogue
import static com.bartslota.legacyrefactoring.entities.BookEntity.BookType.Circulating
import static com.bartslota.legacyrefactoring.entities.BookHolderEntity.HolderType.Regular
import static com.bartslota.legacyrefactoring.entities.BookHolderEntity.HolderType.Researcher
import static com.bartslota.newmodel.lending.domain.patron.Patron.patron
import static java.math.BigDecimal.ZERO

/**
 * @author bslota on 07/12/2021
 */
@Component
class Fixtures {

    @Autowired
    private BookRepository bookRepository

    @Autowired
    private BookHolderRepository bookHolderRepository

    @Autowired
    private com.bartslota.newmodel.lending.domain.book.BookRepository bookNewModelRepo;

    @Autowired
    private PatronRepository patronRepository;


    BookHolderEntity aPatron(BookHolderEntity.HolderType type) {
        BookHolderEntity holder = new BookHolderEntity()
        holder.setType(type)
        holder.setHolderName("name")
        holder = bookHolderRepository.save(holder)
        if (type == Regular) {
            patronRepository.save(patron(PatronType.Regular, new PatronId(holder.getId())));
        } else {
            patronRepository.save(patron(PatronType.Researcher, new PatronId(holder.getId())));
        }
        return holder
    }

    BookHolderEntity aRegularPatron() {
        return aPatron(Regular)
    }

    BookHolderEntity aResearcherPatron() {
        return aPatron(Researcher)
    }

    BookEntity aCatalogueBook(String author, String title) {
        BookEntity book = new BookEntity()
        book.setIsbn("0198526636")
        book.setAuthor(author)
        book.setTitle(title)
        book.setType(Circulating)
        book.setState(JustInCatalogue)
        book.setLendingCostPerDay(ZERO)
        book = bookRepository.save(book)
        return book
    }

    BookEntity aCirculatingBookAvailableForLending() {
        BookEntity book = new BookEntity()
        book.setIsbn("0198526636")
        book.setAuthor("author")
        book.setTitle("title")
        book.setState(InLending)
        book.setLendingState(Available)
        book.setType(Circulating)
        book.setLendingCostPerDay(ZERO)
        book = bookRepository.save(book)
        bookNewModelRepo.save(new Book(new BookId(book.getId()), BookType.Circulating, Book.State.Available));
        return book;
    }

    BookEntity aRestrictedBookAvailableForLending() {
        BookEntity book = new BookEntity()
        book.setIsbn("0198526636")
        book.setAuthor("author")
        book.setTitle("title")
        book.setState(InLending)
        book.setLendingState(Available)
        book.setType(BookEntity.BookType.Restricted)
        book.setLendingCostPerDay(ZERO)
        book = bookRepository.save(book)
        bookNewModelRepo.save(new Book(new BookId(book.getId()), BookType.Restricted, Book.State.Available))
        return book
    }

}

