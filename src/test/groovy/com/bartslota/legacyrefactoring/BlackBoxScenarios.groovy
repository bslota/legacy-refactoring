package com.bartslota.legacyrefactoring

import com.bartslota.acl.AclConfiguration
import com.bartslota.legacyrefactoring.controllers.BookController
import com.bartslota.legacyrefactoring.dtos.BookDto
import com.bartslota.legacyrefactoring.dtos.BookRequest
import com.bartslota.legacyrefactoring.entities.BookEntity
import com.bartslota.legacyrefactoring.entities.BookHolderEntity
import com.bartslota.legacyrefactoring.exceptions.InvalidBookCollectionStateException
import com.bartslota.legacyrefactoring.exceptions.InvalidBookLendingStateException
import com.bartslota.legacyrefactoring.rabbitmq.QueueListener
import com.bartslota.legacyrefactoring.repositories.BookRepository
import com.bartslota.legacyrefactoring.services.HolderRentalFeeService
import org.junit.Rule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.togglz.junit.TogglzRule
import spock.lang.Specification

import java.time.Duration
import java.time.Instant
import java.util.stream.Collectors

import static com.bartslota.legacyrefactoring.entities.BookEntity.BookLendingState.Collected
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic

/**
 * @author bslota on 07/12/2021
 */
@SpringBootTest(classes = [LegacyRefactoringApplication.class, AclConfiguration.class])
class BlackBoxScenarios extends Specification {

    @Autowired
    private BookController bookController

    @Autowired
    private Fixtures fixtures

    @Autowired
    private HolderRentalFeeService holderRentalFeeService

    @Autowired
    private BookRepository bookRepository

    @Autowired
    private QueueListener queueListener

    @Rule
    public TogglzRule togglzRule = TogglzRule.allDisabled(NewModelToggles.class);

    def "patron can hold a book"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToHoldBook(regularPatron, circulatingBook)

        then:
            placedOnHoldsBooksBy(regularPatron).contains(circulatingBook.getId())

    }


    def "patron can hold up to 5 books"() {
        given:
            BookEntity circulatingBook1 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatingBook2 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatingBook3 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatingBook4 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatingBook5 = fixtures.aCirculatingBookAvailableForLending()
            BookEntity circulatingBook6 = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(regularPatron, circulatingBook1, circulatingBook2, circulatingBook3, circulatingBook4, circulatingBook5)

        when:
            patronWantsToHoldBook(regularPatron, circulatingBook6)

        then:
            thrown InvalidBookLendingStateException.class
    }


    def "researcher patron can place open-ended holds"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()
        and:
            BookHolderEntity researcherPatron = fixtures.aResearcherPatron()

        when:
            patronWantsToHoldBookForOpenEndedHold(researcherPatron, circulatingBook)

        then:
            placedOnHoldsBooksBy(researcherPatron).contains(circulatingBook.getId())
    }


    def "regular patron cannot place open-ended holds"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()
        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToHoldBookForOpenEndedHold(regularPatron, circulatingBook)

        then:
            thrown InvalidBookLendingStateException.class
    }


    def "regular patron cannot hold restricted books"() {
        //TODO
    }


    def "researcher patron can hold restricted books"() {
        //TODO
    }


    def "patron cannot hold books when there are two overdue collections"() {
        given:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            patronHasTwoCollectedBooksTillYesterday(regularPatron)

        and:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            patronWantsToHoldBook(regularPatron, book)

        then:
            thrown InvalidBookLendingStateException.class
    }


    def "patron can hold books when there is one overdue collection"() {
        given:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            thereIsABookCollectedTillYesterdayBy(regularPatron)

        and:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            patronWantsToHoldBook(regularPatron, book)

        then:
            placedOnHoldsBooksBy(regularPatron).contains(book.getId())
    }


    def "cannot place on hold a book which is held by someone else"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            BookHolderEntity anotherRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(regularPatron, circulatingBook)

        when:
            patronWantsToHoldBook(anotherRegularPatron, circulatingBook)

        then:
            thrown InvalidBookLendingStateException.class
    }


    def "can collect a book"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(regularPatron, circulatingBook)

        when:
            patronWantsToCollectTheBook(regularPatron, circulatingBook, 5)

        then:
            collectedBooksBy(regularPatron).contains(circulatingBook.getId())

        and:
            placedOnHoldsBooksBy(regularPatron).isEmpty()
    }


    def "cannot collect book not placed on hold"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        when:
            patronWantsToCollectTheBook(regularPatron, circulatingBook, 5)

        then:
            thrown InvalidBookCollectionStateException.class
    }


    def "book can be collected for up to 60 days"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(regularPatron, circulatingBook)

        when:
            patronWantsToCollectTheBook(regularPatron, circulatingBook, 61)

        then:
            thrown InvalidBookCollectionStateException.class
    }


    def "can return a book"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity aRegularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(aRegularPatron, circulatingBook)

        and:
            patronWantsToCollectTheBook(aRegularPatron, circulatingBook, 5)

        when:
            patronWantsToReturnTheBook(aRegularPatron, circulatingBook)

        then:
            collectedBooksBy(aRegularPatron).isEmpty()
            placedOnHoldsBooksBy(aRegularPatron).isEmpty()
    }


    def "incorrect isbn must be rejected"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            someoneChangesISBN(isbn, book)

        then:
            thrown IllegalArgumentException.class

        where:
            isbn << ["", null, "novalidIsbn"]

    }

    def "valid isbn can be set"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            ResponseEntity response = someoneChangesISBN("0123456789", book)

        then:
            response.getStatusCode() == HttpStatus.OK
    }


    def "title must be not empty and not longer than 100 characters"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            someoneChangesTitleTo(title, book)

        then:
            thrown IllegalArgumentException.class

        where:
            title << ["", null, randomAlphabetic(101)]
    }


    def "valid title can be set"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            ResponseEntity response = someoneChangesTitleTo("Valid Title", book)

        then:
            response.getStatusCode() == HttpStatus.OK
    }


    def "author must be not empty and not longer than 60"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            someoneChangesAuthorTo(author, book)

        then:
            thrown IllegalArgumentException.class

        where:
            author << ["", null, randomAlphabetic(61)]
    }


    def "valid author can be set"() {
        given:
            BookEntity book = fixtures.aCirculatingBookAvailableForLending()

        when:
            ResponseEntity response = someoneChangesAuthorTo("Valid Author", book)

        then:
            response.getStatusCode() == HttpStatus.OK
    }


    def "can collect a book listening to rabbitMq"() {
        given:
            BookEntity circulatingBook = fixtures.aCirculatingBookAvailableForLending()

        and:
            BookHolderEntity regularPatron = fixtures.aRegularPatron()

        and:
            patronWantsToHoldBook(regularPatron, circulatingBook)

        when:
            queueListener.collect(new BookRequest(circulatingBook.getId(), regularPatron.getId(), 10, false))

        then:
            collectedBooksBy(regularPatron).contains(circulatingBook.getId())
            placedOnHoldsBooksBy(regularPatron).isEmpty()
    }

    void patronWantsToHoldBook(BookHolderEntity patron, BookEntity... books) {
        Arrays.asList(books).forEach(book -> bookController.addHold(new BookRequest(book.getId(), patron.getId(), 10, false)))
    }

    void patronWantsToHoldBookForOpenEndedHold(BookHolderEntity patron, BookEntity... books) {
        Arrays.asList(books).forEach(book -> bookController.addHold(new BookRequest(book.getId(), patron.getId(), null, true)))
    }

    void patronWantsToCollectTheBook(BookHolderEntity patron, BookEntity book, int days) {
        bookController.collect(new BookRequest(book.getId(), patron.getId(), days, true))
    }

    void patronWantsToReturnTheBook(BookHolderEntity patron, BookEntity book) {
        bookController.removeCollectedBook(new BookRequest(book.getId(), patron.getId(), null, null))
    }


    List<UUID> placedOnHoldsBooksBy(BookHolderEntity regularPatron) {
        return bookController.getPlacedOnHoldBooks(regularPatron.getId()).getBody().stream().map(BookDto::getBookId).collect(Collectors.toList())
    }

    List<UUID> collectedBooksBy(BookHolderEntity aRegularPatron) {
        return bookController.getCollectedBooks(aRegularPatron.getId()).getBody().stream().map(BookDto::getBookId).collect(Collectors.toList())
    }

    ResponseEntity someoneChangesTitleTo(String newTitle, BookEntity book) {
        BookDto dto = BookDto.from(book)
        dto.setTitle(newTitle)
        return bookController.changeDescription(dto)
    }

    ResponseEntity someoneChangesAuthorTo(String author, BookEntity book) {
        BookDto dto = BookDto.from(book)
        dto.setAuthor(author)
        return bookController.changeDescription(dto)
    }

    ResponseEntity someoneChangesISBN(String newIsbn, BookEntity book) {
        BookDto dto = BookDto.from(book)
        dto.setIsbn(newIsbn)
        return bookController.changeState(dto)
    }


    void patronHasTwoCollectedBooksTillYesterday(BookHolderEntity regularPatron) {
        thereIsABookCollectedTillYesterdayBy(regularPatron)
        thereIsABookCollectedTillYesterdayBy(regularPatron)
    }

    void thereIsABookCollectedTillYesterdayBy(BookHolderEntity aRegularPatron) {
        BookEntity bookEntity = fixtures.aCirculatingBookAvailableForLending()
        patronWantsToHoldBook(aRegularPatron, bookEntity)
        patronWantsToCollectTheBook(aRegularPatron, bookEntity, 1)
        bookEntity.setCollectedFrom(Instant.now() - Duration.ofDays(2))
        bookEntity.setCollectedTill(Instant.now() - Duration.ofDays(1))
        bookEntity.setLendingState(Collected)
        bookRepository.save(bookEntity)
    }

}



