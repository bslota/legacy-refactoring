package com.bartslota.acl.reconciliation

import spock.lang.Specification

import java.util.stream.Collectors
import java.util.stream.IntStream

class ReconciliationTest extends Specification {


    def "should return diff"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)
            Set<Integer> oldOne = IntStream.of(1, 2, 3).boxed().collect(Collectors.toSet())
            Set<Integer> newOne = IntStream.of(2, 3, 4).boxed().collect(Collectors.toSet())

        when:
            Diff<Integer> diff = reconciliation.compare(oldOne, newOne)

        then:
            diff.exists()
            diff.getPresentJustInNew().contains(4)
            diff.getPresentJustInOld().contains(1)

    }


    def "should work for empty input"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)

        when:
            Diff<Integer> diff = reconciliation.compare(new HashSet<>(), new HashSet<>())

        then:
            !diff.exists()
            diff.getPresentJustInNew().isEmpty()
            diff.getPresentJustInOld().isEmpty()

    }


    def "should not return diff for same collections"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)
            Set<Integer> oldOne = IntStream.of(1, 2, 3).boxed().collect(Collectors.toSet())
            Set<Integer> newOne = IntStream.of(1, 2, 3).boxed().collect(Collectors.toSet())

        when:
            Diff<Integer> diff = reconciliation.compare(oldOne, newOne)

        then:
            !diff.exists()
            diff.getPresentJustInNew().isEmpty()
            diff.getPresentJustInOld().isEmpty()

    }


    def "should not work for null input"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)

        when:
            reconciliation.compare(null, new HashSet<>())

        then:
            thrown NullPointerException

        when:
            reconciliation.compare(new HashSet<>(), null)

        then:
            thrown NullPointerException
    }


    def "should work when old has less"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)
            Set<Integer> oldOne = IntStream.of(1, 2, 3).boxed().collect(Collectors.toSet())
            Set<Integer> newOne = IntStream.of(1, 2, 3, 4).boxed().collect(Collectors.toSet())

        when:
            Diff<Integer> diff = reconciliation.compare(oldOne, newOne)

        then:
            diff.exists()
            diff.getPresentJustInNew().contains(4)
            diff.getPresentJustInOld().isEmpty()

    }


    def "should work when old has more"() throws Exception {
        given:
            Reconciliation<Integer> reconciliation = new Reconciliation<>(System.out::println)
            Set<Integer> oldOne = IntStream.of(1, 2, 3).boxed().collect(Collectors.toSet())
            Set<Integer> newOne = IntStream.of(1).boxed().collect(Collectors.toSet())

        when:
            Diff<Integer> diff = reconciliation.compare(oldOne, newOne)

        then:
            diff.exists()
            diff.getPresentJustInNew().isEmpty()
            diff.getPresentJustInOld().containsAll(2, 3)

    }

}