package com.bartslota.acl

import com.bartslota.legacyrefactoring.LegacyRefactoringApplication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.togglz.core.manager.FeatureManager
import spock.lang.Specification

import static com.bartslota.acl.toggles.NewModelToggles.RECONCILE_AND_USE_NEW_MODEL

@SpringBootTest(classes = [AclConfiguration, LegacyRefactoringApplication])
class SanityFeatureToggleTest extends Specification {

    @Autowired
    private FeatureManager manager;

    def "make sure new model is disabled on prod"() {
        expect:
            !manager.isActive(RECONCILE_AND_USE_NEW_MODEL)
    }

}