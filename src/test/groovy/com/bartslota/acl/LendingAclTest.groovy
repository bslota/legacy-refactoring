package com.bartslota.acl

import com.bartslota.acl.reconciliation.Reconciliation
import com.bartslota.acl.toggles.NewModelToggles
import com.bartslota.legacyrefactoring.LegacyRefactoringApplication
import com.bartslota.legacyrefactoring.dtos.BookDto
import com.bartslota.newmodel.lending.application.LendingFacade
import com.bartslota.newmodel.lending.application.readmodel.CollectedBooksView
import com.bartslota.newmodel.lending.application.readmodel.PlacedOnHoldBooksView
import com.bartslota.newmodel.lending.domain.patron.PatronId
import org.junit.Rule
import org.mockito.InjectMocks
import org.mockito.Mock
import org.springframework.boot.test.context.SpringBootTest
import org.togglz.junit.TogglzRule
import spock.lang.Specification

import static com.bartslota.acl.toggles.NewModelToggles.RECONCILE_AND_USE_NEW_MODEL
import static com.bartslota.acl.toggles.NewModelToggles.RECONCILE_NEW_MODEL
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.verifyNoInteractions
import static org.mockito.Mockito.when

@SpringBootTest(classes = [AclConfiguration, LegacyRefactoringApplication])
class LendingAclTest extends Specification {

    @Rule
    TogglzRule togglzRule = TogglzRule.allDisabled(NewModelToggles.class)

    @Mock
    Reconciliation<BookDto> reconciliation

    @Mock
    LendingFacade lendingFacade

    @InjectMocks
    LendingACL lendingACL

    UUID patronId = UUID.randomUUID()


    List<BookDto> oldModelResult = new ArrayList<>()
    PlacedOnHoldBooksView placedOnHoldBooksView =
            new PlacedOnHoldBooksView(new PatronId(patronId), new ArrayList<>())
    CollectedBooksView collectedBooksView =
            new CollectedBooksView(new PatronId(patronId), new ArrayList<>())

    List<BookDto> newModelPlacedOnHoldBooks = BookDto.translateFrom(placedOnHoldBooksView)
    List<BookDto> newModelCollectedBooks = BookDto.translateFrom(collectedBooksView)

    def setup() {
        oldModelResult.add(new BookDto())
        oldModelResult.add(new BookDto())
    }

    def cleanup() {
        goodOldFashionedModelIsEnabled()
    }


    def "should just reconcile books placed on hold"() {
        given:
            justReconciliationIsEnabled()

        and:
            newModelReturnsNewResults()

        when:
            List<BookDto> books = askingForBooksPlacedOnHold()

        then:
            assertThat(books).isEqualTo(oldModelResult)

        and:
            reconciliationOfPlacedOnHoldBooksWasDone()
    }


    def "should return new model for books placed on hold"() {
        given:
            newModelIsEnabled()

        when:
            List<BookDto> books = askingForBooksPlacedOnHold()

        then:
            assertThat(books).isEqualTo(newModelPlacedOnHoldBooks)

        and:
            reconciliationOfPlacedOnHoldBooksWasDone()
    }


    def "should return old model and not reconcile for books placed on hold"() {
        given:
            goodOldFashionedModelIsEnabled()

        when:
            List<BookDto> books = askingForBooksPlacedOnHold()

        then:
            assertThat(books).isEqualTo(oldModelResult)

        and:
            noReconciliationWasDone()
    }


    def "should just reconcile collected books"() {
        given:
            justReconciliationIsEnabled()

        when:
            List<BookDto> books = askingForBooksPlacedOnHold()

        then:
            assertThat(books).isEqualTo(oldModelResult)

        and:
            reconciliationOfCollectedBooksWasDone()
    }


    def "should return new model for collected books"() {
        given:
            newModelIsEnabled()

        when:
            List<BookDto> books = askingForCollectedBooks()

        then:
            assertThat(books).isEqualTo(newModelCollectedBooks)

        and:
            reconciliationOfCollectedBooksWasDone()
    }


    def "should old model and not reconcile for collected books"() {
        given:
            goodOldFashionedModelIsEnabled()

        when:
            List<BookDto> books = askingForCollectedBooks()

        then:
            assertThat(books).isEqualTo(oldModelResult)

        and:
            noReconciliationWasDone()
    }

    void noReconciliationWasDone() {
        verifyNoInteractions(reconciliation)
    }

    List<BookDto> askingForBooksPlacedOnHold() {
        return lendingACL.booksPlacedOnHoldBy(patronId, oldModelResult)
    }

    List<BookDto> askingForCollectedBooks() {
        return lendingACL.booksCurrentlyCollectedBy(patronId, oldModelResult)
    }

    void justReconciliationIsEnabled() {
        togglzRule.disable(RECONCILE_AND_USE_NEW_MODEL)
        togglzRule.enable(RECONCILE_NEW_MODEL)
        newModelReturnsNewResults()

    }

    void newModelIsEnabled() {
        togglzRule.enable(RECONCILE_AND_USE_NEW_MODEL)
        togglzRule.enable(RECONCILE_NEW_MODEL)
        newModelReturnsNewResults()
    }

    void goodOldFashionedModelIsEnabled() {
        togglzRule.disable(RECONCILE_AND_USE_NEW_MODEL)
        togglzRule.disable(RECONCILE_NEW_MODEL)
    }

    void newModelReturnsNewResults() {

        when(lendingFacade.booksPlacedOnHoldBy(new PatronId(patronId))).thenReturn(placedOnHoldBooksView)
        when(lendingFacade.booksCollectedBy(new PatronId(patronId))).thenReturn(collectedBooksView)

    }

    void reconciliationOfPlacedOnHoldBooksWasDone() {
        verify(reconciliation).compare(toSet(oldModelResult), toSet(newModelPlacedOnHoldBooks))
    }

    void reconciliationOfCollectedBooksWasDone() {
        verify(reconciliation).compare(toSet(oldModelResult), toSet(newModelPlacedOnHoldBooks))
    }

    Set<BookDto> toSet(List<BookDto> books) {
        return new HashSet<>(books)
    }

}