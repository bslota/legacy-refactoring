package com.bartslota.newmodel.lending.domain.patron

import com.bartslota.newmodel.catalogue.BookId
import com.bartslota.newmodel.lending.domain.book.AvailableBook
import com.bartslota.newmodel.lending.domain.patron.Patron.Holds
import com.bartslota.newmodel.lending.domain.patron.Patron.OverdueCollectedBooks

import static com.bartslota.newmodel.lending.domain.book.BookType.Circulating
import static com.bartslota.newmodel.lending.domain.book.BookType.Restricted
import static com.bartslota.newmodel.lending.domain.patron.PatronType.Regular
import static com.bartslota.newmodel.lending.domain.patron.PatronType.Researcher

class Fixtures {

    static Patron aRegularPatron() {
        return new Patron(new PatronId(UUID.randomUUID()), Regular, Holds.empty(), OverdueCollectedBooks.empty());
    }

    static Patron aResearcherPatron() {
        return new Patron(new PatronId(UUID.randomUUID()),
                Researcher,
                Holds.empty(),
                OverdueCollectedBooks.empty());
    }

    static Patron aResearcherPatronWithTwoOverdueBooks() {
        return new Patron(new PatronId(UUID.randomUUID()),
                Researcher,
                Holds.empty(),
                OverdueCollectedBooks.from(anyBookId(), anyBookId()));
    }

    static BookId anyBookId() {
        return new BookId(UUID.randomUUID());
    }

    static AvailableBook restrictedBook() {
        return new AvailableBook(anyBookId(), Restricted);
    }

    static AvailableBook circulatingBook() {
        return new AvailableBook(anyBookId(), Circulating);
    }
}

