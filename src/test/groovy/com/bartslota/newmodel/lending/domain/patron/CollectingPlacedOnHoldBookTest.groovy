package com.bartslota.newmodel.lending.domain.patron

import com.bartslota.newmodel.lending.domain.book.AvailableBook
import com.bartslota.newmodel.lending.domain.patron.events.BookCollected
import spock.lang.Specification

import java.time.Period

import static com.bartslota.newmodel.lending.domain.patron.CollectDuration.forOneMonth
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.aRegularPatron
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.circulatingBook
import static com.bartslota.newmodel.lending.domain.patron.HoldDuration.forTenDays

class CollectingPlacedOnHoldBookTest extends Specification {

    /**
     * TODO: to be implemented
     */
    def 'can collect an existing hold'() {
        given:
            Patron patron = aRegularPatron()
        and:
            AvailableBook book = circulatingBook()
        and:
            patron.placeOnHold(book, forTenDays())
        when:
            Optional<BookCollected> event = patron.collect(book.bookId(), forOneMonth())
        then:
            event.isPresent()
    }

    def 'cannot collect when book is not on hold'() {
        given:
            Patron patron = aRegularPatron()

        and:
            AvailableBook book = circulatingBook()

        when:
            Optional<BookCollected> event = patron.collect(book.bookId(), forOneMonth())

        then:
            !event.isPresent()
    }

    def 'collect duration cannot exceed 60 days'() {
        when:
            aRegularPatron().collect(circulatingBook().bookId(), forDays(days))

        then:
            thrown IllegalArgumentException

        where:
            days << (61..100)
    }

    def 'collect duration can be up to 60 days'() {
        when:
            aRegularPatron().collect(circulatingBook().bookId(), forDays(days))

        then:
            noExceptionThrown()

        where:
            days << (1..60)
    }

    static CollectDuration forDays(int days) {
        return new CollectDuration(Period.ofDays(days))
    }
}
