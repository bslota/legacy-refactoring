package com.bartslota.newmodel.lending.domain.patron

import com.bartslota.newmodel.lending.domain.patron.events.BookPlacedOnHold
import spock.lang.Specification

import static com.bartslota.newmodel.lending.domain.patron.Fixtures.aRegularPatron
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.aResearcherPatron
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.circulatingBook
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.restrictedBook

class PlacingOnHoldBookTest extends Specification {

    def 'can place on hold circulating book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(circulatingBook(), HoldDuration.forTenDays())

        then:
            event.isPresent()
    }

    def 'cannot place on hold restricted book if patron is regular'() {
        given:
            Patron patron = aRegularPatron()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(restrictedBook(), HoldDuration.forTenDays())

        then:
            !event.isPresent()
    }

    def 'can place on hold restricted book if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(restrictedBook(), HoldDuration.forTenDays())

        then:
            event.isPresent()
    }

    def 'can place on hold up to 5 books'() {
        given:
            Patron patron = aResearcherPatron()

        and:
            5.times { patron.placeOnHold(circulatingBook(), HoldDuration.forTenDays()) }

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(circulatingBook(), HoldDuration.forTenDays())

        then:
            !event.isPresent()
    }

    def 'cannot place on hold when there are 2 overdue collected books'() {
        given:
            Patron patron = Fixtures.aResearcherPatronWithTwoOverdueBooks()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(circulatingBook(), HoldDuration.forTenDays())

        then:
            !event.isPresent()
    }

    def 'cannot place open-ended hold if patron is regular'() {
        given:
            Patron patron = aRegularPatron()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(circulatingBook(), new OpenEndedHoldDuration())

        then:
            !event.isPresent()
    }

    def 'can place open-ended hold if patron is researcher'() {
        given:
            Patron patron = aResearcherPatron()

        when:
            Optional<BookPlacedOnHold> event = patron.placeOnHold(circulatingBook(), new OpenEndedHoldDuration())

        then:
            event.isPresent()
    }
}
