package com.bartslota.newmodel.lending.application

import com.bartslota.newmodel.DomainEvents
import com.bartslota.newmodel.lending.domain.book.AvailableBook
import com.bartslota.newmodel.lending.domain.patron.Patron
import com.bartslota.newmodel.lending.domain.patron.PatronRepository
import com.bartslota.newmodel.lending.domain.patron.events.BookCollected
import com.bartslota.newmodel.lending.infrastructure.InMemoryPatronRepository
import spock.lang.Specification

import static com.bartslota.newmodel.lending.domain.patron.CollectDuration.forOneMonth
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.aRegularPatron
import static com.bartslota.newmodel.lending.domain.patron.Fixtures.circulatingBook
import static com.bartslota.newmodel.lending.domain.patron.HoldDuration.forTenDays

class CollectServiceTest extends Specification {

    private PatronRepository patronRepository = new InMemoryPatronRepository()
    private DomainEvents events = Mock()
    private CollectService collectService = new CollectService(patronRepository, events)

    def "should publish an event when operation was successful"() {
        given:
            Patron patron = persistedRegularPatron()

        and:
            AvailableBook book = circulatingBook()

        and:
            patron.placeOnHold(book, forTenDays())

        when:
            collectService.placeOnHold(new CollectCommand(forOneMonth(), book.bookId(), patron.getPatronId()))

        then:
            1 * events.publish(_ as BookCollected)

    }

    def "should not publish an event when operation was not successful"() {
        given:
            Patron patron = persistedRegularPatron()

        and:
            AvailableBook book = circulatingBook()

        when:
            collectService.placeOnHold(new CollectCommand(forOneMonth(), book.bookId(), patron.getPatronId()))

        then:
            0 * events.publish(_)
    }

    Patron persistedRegularPatron() {
        Patron patron = aRegularPatron()
        patronRepository.save(patron)
        return patron
    }

}