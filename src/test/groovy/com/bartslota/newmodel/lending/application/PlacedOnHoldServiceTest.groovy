package com.bartslota.newmodel.lending.application

import com.bartslota.newmodel.DomainEvents
import com.bartslota.newmodel.lending.domain.book.AvailableBook
import com.bartslota.newmodel.lending.domain.patron.Fixtures
import com.bartslota.newmodel.lending.domain.patron.Patron
import com.bartslota.newmodel.lending.domain.patron.PatronRepository
import com.bartslota.newmodel.lending.domain.patron.events.BookPlacedOnHold
import com.bartslota.newmodel.lending.infrastructure.InMemoryPatronRepository
import spock.lang.Specification

import static com.bartslota.newmodel.lending.domain.patron.Fixtures.aRegularPatron
import static com.bartslota.newmodel.lending.domain.patron.HoldDuration.forTenDays

class PlacedOnHoldServiceTest extends Specification {

    private PatronRepository patronRepository = new InMemoryPatronRepository()
    private FindAvailableBook findAvailableBook = Stub(FindAvailableBook)
    private DomainEvents events = Mock(DomainEvents)
    private PlaceOnHoldService placeOnHoldService = new PlaceOnHoldService(patronRepository, findAvailableBook, events)


    def "should publish an event when operation was successful"() {
        given:
            Patron patron = persistedRegularPatron()

        and:
            AvailableBook book = availableBook()

        when:
            placeOnHoldService.placeOnHold(new PlaceOnHoldCommand(book.bookId(), patron.getPatronId(), forTenDays()))

        then:
            1 * events.publish(_ as BookPlacedOnHold)
    }


    def "should not publish an event when operation was not successful"() {
        given:
            Patron patron = persistedRegularPatron()

        and:
            AvailableBook book = availableRestrictedBook()

        when:
            placeOnHoldService.placeOnHold(new PlaceOnHoldCommand(book.bookId(), patron.getPatronId(), forTenDays()))

        then:
            0 * events.publish(_)
    }

    Patron persistedRegularPatron() {
        Patron patron = aRegularPatron()
        patronRepository.save(patron)
        return patron
    }

    AvailableBook availableBook() {
        AvailableBook book = Fixtures.circulatingBook()
        findAvailableBook.find(_) >> Optional.of(book)
        return book
    }

    AvailableBook availableRestrictedBook() {
        AvailableBook book = Fixtures.restrictedBook()
        findAvailableBook.find(_) >> Optional.of(book)
        return book
    }

}