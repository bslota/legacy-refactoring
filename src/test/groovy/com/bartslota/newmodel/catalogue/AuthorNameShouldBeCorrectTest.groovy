package com.bartslota.newmodel.catalogue

import org.apache.commons.lang3.RandomStringUtils
import spock.lang.Specification

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic

class AuthorNameShouldBeCorrectTest extends Specification {

    def "author should not be null"() {
        when:
            new Author(null)

        then:
            thrown NullPointerException
    }


    def "author should not be longer than 60"() {
        when:
            new Author(randomAlphabetic(61))

        then:
            thrown IllegalArgumentException
    }

}


