package com.bartslota.newmodel.catalogue


import spock.lang.Specification

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic

class TitleShouldBeCorrectScenarios extends Specification {

    def "title should not be null"() {
        when:
            new Title(null)

        then:
            thrown NullPointerException
    }

    def "title should not be longer than 100"() {
        when:
            new Title(randomAlphabetic(101))

        then:
            thrown IllegalArgumentException
    }

}


